 Теоријски део испита из предмета ``Пројектовање електрониских уређаја на системском нивоу''

#### 1. Предавање - Zynq

1. Поређење "hard" и "soft" процесора
2. Архитектура "PS" дела "Zynq" платформе
3. Архитектура "APU" дела "Zynq" платформе
4. Архитектура "PL" дела "Zynq" платформе, навести делове и укратко
   објаснити
5. Специјализовани делови "PL" дела "Zynq" платформе, навести који
   постоје и укратко објаснити
6. Интерфејси између "PL" и "PS" дела "Zynq" платформе, навести типове
   и укратко објаснити

#### 2. Предавање - Model

1. Шта је модел и значај модела
2. Врсте модела
3. Модели израчунавања
4. Упрошћавање, навести врсте и укратко објаснити
5. Модели и језици
6. Таксономија ESL-a, објаснити шта је, навести све осе
7. Временска оса ESL таксономије
8. Оса података ESL таксономије
9. Оса конкурентности ESL таксономије
10. Оса комуникације ESL таксономије
11. Однос оса конкурентности и комуникације ESL таксономије
12. Оса конфигурације ESL таксономије
13. Навести све осе ESL таксономије и тачке на њима
14. Илустровати ESL таксономију на примеру функционалног модела
15. Илустровати ESL таксономију на примеру HDL описа
16. Илустровати ESL таксономију на примеру FPGA решења
17. Илустровати ESL таксономију на примеру уопштеног C решења
18. Илустровати ESL таксономију на примеру SystemC решења

#### 3. Предавање - Evolution

1. Мотивација за ESL методологију
2. Потенцијалне вредности система за виртуелни прототип "VSP"
3. "VSP" - Навести три погледа на систем и укратко објаснити
4. Препреке у усвајању бихевијалног моделовања, навести разлоге и
   укратко објаснити сваки од њих
5. Аутоматска имплементација програмабилног хардвера

#### 4. Предавање - Enablers

1. Потребе и захтеви систем дизајнера
2. Потребна прецизност за систем дизајнере
3. Брзина и време за систем дизајнере
4. Потребе и захтеви софтверског тима
5. Потребна прецизност и регистарск прецизност за софтверски тим
6. Перформансе модела који се извршава за софтверски тим, класификација
   модела на основу перформанси
7. Интерпретирани самостални модели за софтверски тим
8. Интерпретирани слејв модели за софтверски тим
9. "Cache Line Just-In-Time" модели за софтверски тим
10. "Cache Page JIT" модели за софтверски тим
11. Модели компајлирани на "Host" машини за софтверски тим
12. Потребе и захтеви хардверског тима
13. Верификациони захтеви хардверског тима

#### 5. Предавање - Flow

1. "ESL" ток - репрезентација дизајна током развоја, навести нивое и
   објаснити
2. "ESL" ток - циљ дизајна
3. "ESL" ток - навести кораке у развоју и објаснити могуће приступе
   имплементацији корака
4. "ESL" ток - објаснити укратко корак спецификације и моделовања,
   навести врсте псецификације и неке језике за моделовање који се
   користе у овом кораку
5. "ESL" ток - објаснити укратко корак анализе пре партиционисања
6. "ESL" ток - објаснити укратко корак партиционисања дизајна
7. "ESL" ток - објаснити укратко корак анализе након партиционисања и
   отклањања грешака
8. "ESL" ток - објаснити укратко корак верификације након
   партиционисања
9. "ESL" ток - објаснити укратко корак хардверске имплементације
10. "ESL" ток - објаснити проблеме са бихевијалном синтезом
11. "ESL" ток - објаснити предности "ESL" синтезе
12. "ESL" ток - објаснити укратко корак софтверске имплементације
13. "ESL" ток - објаснити укратко корак верификације имплементације
14. "ESL" ток - проблеми

#### 6. Предавање - Specification and Modeling

1. Менаџмент захтева и спецификација на папиру
2. "ESL" домени - "Dataflow and Control Flow"
3. "ESL" домени - Протокол стек
4. "ESL" домени - Embedded системи
5. Спецификација која може да се извршава, објаснити и набројати језике
   за овакву врсту спецификације
6. Објаснити развој заснован на профињавању модела

#### 7. Предавање - Pre-Partitioning Analysis

1. Статичка анализа пре партиционисања
2. Анализа пре партиционисања у случају коришћења платформи
3. Динамичка анализа пре партиционисања
4. Алгоритамска анализа пре партиционисања
5. Коришћење резултата анализе пре партиционисања

#### 8. Предавање - Partitioning

1. Партиционисање, кластерисање и абстракција, илустровати. Објаснити
   платформе у овом контексту.
2. Кораци при партиционисању
3. Функционална декомпозиција при партиционисању
4. Опис архитектуре при партиционисању
5. Платформе при партиционисању
6. Додела при партиционисању, навести стратегије и објаснити их
7. Хардверско партиционисање
8. Софтверско партиционисање
9. Софтверско партиционисање на мултипроцесорима и задацима
10. Софтверско партиционисање у оперативним системима

#### 9. Предавање - Post-Partitioning Analysis

1. Одговорности интерфејса и одржавање модела након
   партиционисања. Илустровати и укратко објаснити
2. Објаснити шта је хардверско и софтверско моделовање и ко-моделовање
   и навети врсте
3. Хардверско софтверско моделовање - јединствен модел
4. Хардверско софтверско моделовање - филтрирање и транслирање
5. Хардверско софтверско моделовање - посебни модели
6. Компоненете које се користе након партиционисања. Илустровати на
   примеру.
7. Динамичка и статичка анализа након партиционисања. Кратко објаснити
   и навести вврсте анализа.
8. Функционална анализа након партиционисања
9. Анализа перформанси након партиционисања
10. Анализа интерфејса након партиционисања
11. Анализа снаге након партиционисања
12. Анализа површине након партиционисања
13. Анализа цене након партиционисања
14. Анализа могућности проналажења грешки након партиционисања

#### 10. Предавање - Post-Partitioning Verification

1. Гледишта на верификацију
2. Верификациони план
3. Анализа специфијације
4. Дизајн модела покривености на високом нивоу
5. Детаљан дизајн модела покривености
6. Хибридне метрике модела покривености
7. Решење верификационог проблема
8. Стимулус генератори
9. Чекер компоненте